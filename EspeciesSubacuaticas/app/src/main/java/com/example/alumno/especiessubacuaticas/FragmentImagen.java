package com.example.alumno.especiessubacuaticas;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentImagen extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.activity_segunda, container, false);
    }

    public void mostrarImagen(int imagen) {
        TouchImageView imagenSeleccionada = getView().findViewById(R.id.imageViewImagenSelecionada);
        imagenSeleccionada.setImageResource(imagen);
    }
}
