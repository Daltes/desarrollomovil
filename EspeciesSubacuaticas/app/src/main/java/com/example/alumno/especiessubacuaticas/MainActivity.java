package com.example.alumno.especiessubacuaticas;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity  implements FragmentListado.objetoListaListener{

    private static final int SECONDARY_ACTIVITY_TAG = 1;

    // Se añade la barra de menu
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Se añade el icono en el menu
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_icono);


        // Se crea el fragmenListado
       FragmentListado frgListado = (FragmentListado)getSupportFragmentManager().findFragmentById(R.id.FrgListado);
       frgListado.setCorreosListener(this);

    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menuItemAcercaDe:
                AlertDFragment alertdFragment = new AlertDFragment();
                // Show Alert DialogFragment
                alertdFragment.show(getSupportFragmentManager(), "Alert Dialog Fragment");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onObjetoListaSeleccionado(ObjetoLista o) {
        boolean hayDetalle =
                (getSupportFragmentManager().findFragmentById(R.id.FrgImagen) != null);

        if(hayDetalle) {
            ((FragmentImagen)getSupportFragmentManager()
                    .findFragmentById(R.id.FrgImagen)).mostrarImagen(o.getRutaImgane());
        }
        else {
            Intent i = new Intent(this, SegundaActivity.class);
            i.putExtra("imagen",o.getRutaImgane());
            i.putExtra("nombre",o.getNombreComun());
            i.putExtra("nombreLatin",o.getNombreLatin());
            startActivityForResult(i, SECONDARY_ACTIVITY_TAG);
        }

    }
}
