package com.example.alumno.especiessubacuaticas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

public class AlertDFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                // Set Dialog Icon
                .setIcon(R.mipmap.ic_alert)
                // Set Dialog Title
                .setTitle(getResources().getString(R.string.alertTitulo))
                // Set Dialog Message
                .setMessage(getResources().getString(R.string.alertContenido))

                // Positive button
                .setPositiveButton(getResources().getString(R.string.alertAceptar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })

                // Negative Button
                .setNegativeButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,    int which) {

                    }
                }).create();
    }
}
