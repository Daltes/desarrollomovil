package com.example.alumno.especiessubacuaticas;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class FragmentListado extends Fragment {

    private ArrayList<ObjetoLista> datosPeces = new ArrayList<ObjetoLista>();
    private ArrayList<ObjetoLista> datosAlgas = new ArrayList<ObjetoLista>();
    private ArrayList<ObjetoLista> datosAdaptador = new ArrayList<ObjetoLista>();
    private boolean esListaPeces = false;
    private objetoListaListener listener;
    ListView _listaInformacion;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragmet_listado, container, false);
    }


    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        //Asignamos el objeto ListView con el id de la etiqueta ListView del
        //layout principal
        _listaInformacion = getView().findViewById(R.id.listViewInformacion);

        // Se selecciona el fichero peces de la carpeta RAW
        InputStream frawPeces = getResources().openRawResource(R.raw.peces);
        InputStream frawAlgas = getResources().openRawResource(R.raw.algaseinvertebrados);

        // Lectura del fichero
        try {
            BufferedReader brPeces = new BufferedReader(new InputStreamReader(frawPeces));
            BufferedReader brAlgas = new BufferedReader(new InputStreamReader(frawAlgas));
            String linea;
            // Se leen los peces
            while ((linea = brPeces.readLine()) != null) {
                String[] contenidoLinea = linea.split(",");

                // Se busca la imagen
                int imgID = getResources().getIdentifier(contenidoLinea[0].toLowerCase().trim(), "drawable", getContext().getPackageName());

                // Se añade al objeto con los datos
                datosPeces.add(new ObjetoLista(imgID, contenidoLinea[1], contenidoLinea[2], contenidoLinea[3], contenidoLinea[4]));
            }

            frawPeces.close();
            brPeces.close();

            // Se leen las algaseinvertebrados
            while ((linea = brAlgas.readLine()) != null) {
                String[] contenidoLinea = linea.split(",");

                int imgID = getResources().getIdentifier(contenidoLinea[0].toLowerCase().trim(), "drawable", getContext().getPackageName());

                datosAlgas.add(new ObjetoLista(imgID, contenidoLinea[1], contenidoLinea[3], contenidoLinea[4], contenidoLinea[5]));
            }

            frawAlgas.close();
            brAlgas.close();

        } catch (Exception e) {
            Log.e("Ficheros", "Error al leer fichero desde recurso raw");
        }

        // Usamos un adaptador para dibujar las opciones de la lista
        final AdaptadorObjetoLista adaptador = new AdaptadorObjetoLista(this, datosAdaptador);

        // Establecemos el adaptador del Listview
        _listaInformacion.setAdapter(adaptador);

        // Se recoge el spinner
        Spinner _SpinnerOpcion = getView().findViewById(R.id.spinnerOpciones);
        // Se recoge el titulo
        final TextView _titulo = getView().findViewById(R.id.textViewTitulo);
        // Se añade el evento
        _SpinnerOpcion.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent,
                                               android.view.View v, int position, long id) {
                        // Se limpia el array
                        datosAdaptador.clear();
                        // Se establece que dataSet se va a usar
                        if (esListaPeces) {
                            _titulo.setText(R.string.tituloAlgas);
                            datosAdaptador.addAll(datosAlgas);
                        } else {
                            _titulo.setText(R.string.tituloPeces);
                            datosAdaptador.addAll(datosPeces);
                        }
                        // Se actualiza la lista
                        adaptador.notifyDataSetChanged();
                        // Se invierte el valor del booleano que indica que dataset usar
                        esListaPeces = !esListaPeces;
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

        _listaInformacion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> list, View view, int pos, long id) {
                if (listener!=null) {
                    listener.onObjetoListaSeleccionado(
                            (ObjetoLista) _listaInformacion.getAdapter().getItem(pos));
                }
            }
        });


    }


    public interface objetoListaListener {
        void onObjetoListaSeleccionado(ObjetoLista o);
    }

    public void setCorreosListener (objetoListaListener listener){
        this.listener = listener;
    }
}
