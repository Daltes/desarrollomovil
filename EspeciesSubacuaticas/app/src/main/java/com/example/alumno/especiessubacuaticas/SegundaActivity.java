package com.example.alumno.especiessubacuaticas;

import android.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;

public class SegundaActivity extends AppCompatActivity {

    // Se añade la barra de menu
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagen);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        int imagen = getIntent().getIntExtra("imagen", 0);
        String nombre = getIntent().getStringExtra("nombre");
        String nombreLatin = getIntent().getStringExtra("nombreLatin");

        FragmentImagen _imagen = (FragmentImagen)getSupportFragmentManager().findFragmentById(R.id.FrgImagen);
        _imagen.mostrarImagen(imagen);
        setTitle(nombre + " (" + nombreLatin + ")");

    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.menuItemAcercaDe:
                AlertDFragment alertdFragment = new AlertDFragment();
                // Show Alert DialogFragment
                alertdFragment.show(getSupportFragmentManager(), "Alert Dialog Fragment");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
