package com.example.alumno.especiessubacuaticas;

public class ObjetoLista {

    int rutaImgane;
    String nombreComun;
    String nombreLatin;
    String longitud;
    String habitat;


    public ObjetoLista(int rutaImagen, String nombreComun, String nombreLatin, String longitud, String habitat) {
        this.rutaImgane = rutaImagen;
        this.nombreComun = nombreComun;
        this.nombreLatin = nombreLatin;
        this.longitud = longitud;
        this.habitat = habitat;
    }

    public int getRutaImgane() {
        return rutaImgane;
    }

    public void setRutaImgane(int rutaImgane) {
        this.rutaImgane = rutaImgane;
    }

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getNombreLatin() {
        return nombreLatin;
    }

    public void setNombreLatin(String nombreLatin) {
        this.nombreLatin = nombreLatin;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }
}
