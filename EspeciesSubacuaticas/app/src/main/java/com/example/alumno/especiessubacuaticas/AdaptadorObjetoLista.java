package com.example.alumno.especiessubacuaticas;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptadorObjetoLista extends ArrayAdapter<ObjetoLista> {

    Activity contexto;

    // Contructor del adaptador usando el contexto de la aplicacion actual
    AdaptadorObjetoLista(Fragment contexto, ArrayList<ObjetoLista> datos) {
        super(contexto.getActivity(), R.layout.list_view_informacion, datos);
        this.contexto =contexto.getActivity();
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        View item = convertView;

        ViewHolder holder;

        if (item == null) {
            LayoutInflater inflater = contexto.getLayoutInflater();
            item = inflater.inflate(R.layout.list_view_informacion, null);

            holder = new ViewHolder();
            holder._nombreComun = item.findViewById(R.id.textViewNombre);
            holder._imagen = item.findViewById(R.id.imageViewfoto);
            holder._nombreLatin = item.findViewById(R.id.textViewNombreLatin);
            holder._longitud = item.findViewById(R.id.textViewTamano);
            holder._habitat = item.findViewById(R.id.textViewHabitat);
            item.setTag(holder);
        } else {
            holder = (ViewHolder) item.getTag();
        }

        //Mediante getItem cargamos cada uno de los objetos del array
        ObjetoLista mielemento = getItem(position);

        // Asignamos los valores
        holder._nombreComun.setText(mielemento.getNombreComun());
        holder._imagen.setImageResource(mielemento.getRutaImgane());
        holder._nombreLatin.setText(mielemento.getNombreLatin());
        holder._longitud.setText(mielemento.getLongitud() + contexto.getResources().getString(R.string.cm));
        holder._habitat.setText(mielemento.getHabitat());

        return item;
    }


    class ViewHolder {
        ImageView _imagen;
        TextView _nombreComun;
        TextView _nombreLatin;
        TextView _longitud;
        TextView _habitat;
    }

}
