package com.example.dany_.gestornotas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class GestionarNota extends AppCompatActivity {

    Spinner _spinnerTipo;
    EditText _titulo;
    EditText _descripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editar_nota);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        _spinnerTipo = findViewById(R.id.spinnerTipo);
        _titulo = findViewById(R.id.editTextTitulo);
        _descripcion = findViewById(R.id.editTextDescripcion);

        String titulo = getIntent().getStringExtra(NotasBDAdapter.CAMPO_TITULO);

        setResult(RESULT_CANCELED);

        if (titulo != null) {
            String descripcion = getIntent().getStringExtra(NotasBDAdapter.CAMPO_DESCRIPCION);
            String tipo = getIntent().getStringExtra(NotasBDAdapter.CAMPO_CATEGORIA);

            _titulo.setText(titulo);
            _descripcion.setText(descripcion);
            if (tipo.equals("REUNIÓN")) {
                _spinnerTipo.setSelection(0);
            } else if (tipo.equals("AVISO")) {
                _spinnerTipo.setSelection(1);
            } else {
                _spinnerTipo.setSelection(2);
            }

        }

        Button _botonAceptar = findViewById(R.id.buttonAceptar);

        _botonAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResponder(v);
            }
        });
    }

    public void onResponder(View v) {

        String titulo = _titulo.getText().toString();
        String descripcion = _descripcion.getText().toString();

        if (titulo.isEmpty() || descripcion.isEmpty()) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.campoVacio), Toast.LENGTH_LONG).show();
            return;
        }

        String tipo;
        if (_spinnerTipo.getSelectedItemPosition() == 0) {
            tipo = "REUNIÓN";
        } else if (_spinnerTipo.getSelectedItemPosition() == 1) {
            tipo = "AVISO";
        } else {
            tipo = "VARIOS";
        }

        // Creamos un nuevo intent donde enviaremos de vuelta
        // los datos de nuestra ejecución.
        Intent datos = new Intent();
        // Metemos como datos extra del intent la respuesta del
        // usuario.

        datos.putExtra(NotasBDAdapter.CAMPO_TITULO, titulo);
        datos.putExtra(NotasBDAdapter.CAMPO_DESCRIPCION, descripcion);
        datos.putExtra(NotasBDAdapter.CAMPO_CATEGORIA, tipo);
        datos.putExtra(NotasBDAdapter.CAMPO_ID, getIntent().getIntExtra(NotasBDAdapter.CAMPO_ID, -1));

        // Le decimos a Android que estamos preparados para acabar
        // con éxito...
        setResult(RESULT_OK, datos);

        // ... y le pedimos que nos cierre.
        finish();

    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
