package com.example.dany_.gestornotas;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class NotasBDHelper extends SQLiteOpenHelper {

    private static final String BD_NOMBRE = "bdnotas.bd";
    private static final int BD_VERSION = 1;

    private static final String BD_CREAR = "CREATE TABLE " + NotasBDAdapter.TABLA_BD + " (" +
            NotasBDAdapter.CAMPO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            NotasBDAdapter.CAMPO_CATEGORIA + " VARCHAR(255) NOT NULL, " +
            NotasBDAdapter.CAMPO_TITULO + " VARCHAR(255) NOT NULL, " +
            NotasBDAdapter.CAMPO_DESCRIPCION + " VARCHAR(255) NOT NULL, " +
            NotasBDAdapter.CAMPO_ICONO + " INTEGER(255) NOT NULL" +
            ")";

    public NotasBDHelper(Context context) {
        super(context, BD_NOMBRE, null, BD_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(BD_CREAR);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + NotasBDAdapter.TABLA_BD);
        db.execSQL(BD_CREAR);
    }
}
