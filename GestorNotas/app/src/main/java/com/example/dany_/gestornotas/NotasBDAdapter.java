package com.example.dany_.gestornotas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class NotasBDAdapter {

    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_CATEGORIA = "categoria";
    public static final String CAMPO_TITULO = "titulo";
    public static final String CAMPO_DESCRIPCION = "descripcion";
    public static final String CAMPO_ICONO = "icono";
    public static final String TABLA_BD = "notas";
    private Context contexto;
    private SQLiteDatabase baseDatos;
    private NotasBDHelper bdHelper;

    public NotasBDAdapter(Context context) {
        this.contexto = context;
    }

    public NotasBDAdapter abrir() throws SQLException {
        NotasBDHelper usdbh =
                new NotasBDHelper(contexto);

        this.baseDatos = usdbh.getWritableDatabase();
        return this;
    }

    public void cerrar() {
        bdHelper.close();
    }

    public long crearNota(String categoria, String titulo, String descripcion, int icono) {
        ContentValues nuevaNota = new ContentValues();
        nuevaNota.put(CAMPO_CATEGORIA, categoria);
        nuevaNota.put(CAMPO_TITULO, titulo);
        nuevaNota.put(CAMPO_DESCRIPCION, descripcion);
        nuevaNota.put(CAMPO_ICONO, icono);
        return baseDatos.insert(TABLA_BD, null, nuevaNota);
    }

    public boolean actualizarNota(long id, String categoria, String titulo, String descripcion, int icono) {
        ContentValues nuevaNota = new ContentValues();
        nuevaNota.put(CAMPO_CATEGORIA, categoria);
        nuevaNota.put(CAMPO_TITULO, titulo);
        nuevaNota.put(CAMPO_DESCRIPCION, descripcion);
        nuevaNota.put(CAMPO_ICONO, icono);
        return baseDatos.update(TABLA_BD, nuevaNota, CAMPO_ID + "=" + id, null) != 0;
    }

    public boolean borrarNota(long id) {
        return baseDatos.delete(TABLA_BD, CAMPO_ID + "=" + id, null) != 0;
    }

    public Cursor obtenerNotas() {
        return baseDatos.rawQuery("SELECT * FROM " + TABLA_BD, null);
    }

    public Cursor getNota(long id) throws SQLException {
        return baseDatos.rawQuery("SELECT * FROM " + TABLA_BD + " WHERE " + CAMPO_ID + "=" + id, null);
    }

}
