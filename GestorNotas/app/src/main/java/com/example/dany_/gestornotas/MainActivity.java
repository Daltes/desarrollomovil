package com.example.dany_.gestornotas;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    // ListView y gridView
    private ListView listViewNotas;
    private GridView gridViewNotas;
    private NotasBDAdapter baseDatos;

    private Intent i;
    private static final int SECONDARY_ACTIVITY_TAG = 1;

    private Cursor contenido;
    private int opcion;

    private boolean esNuevaNota;

    // Botones del menu y variables
    private MenuItem itemListView;
    private MenuItem itemGridView;
    private final int SWITCH_TO_LIST_VIEW = 0;
    private final int SWITCH_TO_GRID_VIEW = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_icono);

        listViewNotas = findViewById(R.id.listViewNotas);
        gridViewNotas = findViewById(R.id.gridViewNotas);

        esNuevaNota = true;

        opcion = SWITCH_TO_LIST_VIEW;

        // Se inicia la base de datos
        baseDatos = new NotasBDAdapter(getApplicationContext());
        baseDatos = baseDatos.abrir();

        //  baseDatos.crearNota("AVISO", "Dentista", "Caduca a fin de mes", R.mipmap.ic_aviso);
        //  baseDatos.crearNota("REUNIÓN", "Cena amigos", "Sábado hay cena", R.mipmap.ic_reunion);
        //  baseDatos.crearNota("VARIOS", "Tutoria", "tutoria el lunes", R.mipmap.ic_varios);
        //  baseDatos.crearNota("REUNIÓN", "R. de departametno", "Sábado hay cena", R.mipmap.ic_reunion);

        establecerAdaptadores();


        registerForContextMenu(listViewNotas);
        registerForContextMenu(gridViewNotas);

        FloatingActionButton _anadir = findViewById(R.id.floatingActionButton2);

        _anadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(MainActivity.this, GestionarNota.class);
                esNuevaNota = true;
                startActivityForResult(i, SECONDARY_ACTIVITY_TAG);
            }
        });

        // Adjuntando el mismo método click para ambos
        this.listViewNotas.setOnItemClickListener(this);
        this.gridViewNotas.setOnItemClickListener(this);

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        i = new Intent(MainActivity.this, GestionarNota.class);

        Cursor c = baseDatos.getNota(id);
        c.moveToFirst();
        i.putExtra(NotasBDAdapter.CAMPO_TITULO, c.getString(2));
        i.putExtra(NotasBDAdapter.CAMPO_DESCRIPCION, c.getString(3));
        i.putExtra(NotasBDAdapter.CAMPO_CATEGORIA, c.getString(1));
        i.putExtra(NotasBDAdapter.CAMPO_ID, c.getInt(0));

        esNuevaNota = false;

        startActivityForResult(i, SECONDARY_ACTIVITY_TAG);
    }

    private void establecerAdaptadores() {

        contenido = baseDatos.obtenerNotas();

        TextView mensajeVacio = findViewById(R.id.textViewMensajeVacio);

        if (contenido.getCount() == 0) {
            mensajeVacio.setVisibility(View.VISIBLE);
            listViewNotas.setVisibility(View.GONE);
            gridViewNotas.setVisibility(View.GONE);

        } else {
            mensajeVacio.setVisibility(View.GONE);
            switchListGridView(opcion);
            // Se recogen el nombre de columnas de la bbdd
            String[] from = new String[]{NotasBDAdapter.CAMPO_TITULO, NotasBDAdapter.CAMPO_CATEGORIA, NotasBDAdapter.CAMPO_ICONO, NotasBDAdapter.CAMPO_ID};
            // Se recogen los elementos a los que se le asignara un valor
            int[] to = new int[]{R.id.textViewNombre, R.id.textViewTipo, R.id.imageViewIcono};
            // Se crea el adaptador para el list
            // Adaptadores
            SimpleCursorAdapter adapterList = new SimpleCursorAdapter(getApplicationContext(), R.layout.fila_notas, contenido, from, to, 1);
            // Se crea el adaptador para el grid
            SimpleCursorAdapter adapterGrid = new SimpleCursorAdapter(getApplicationContext(), R.layout.grid_notas, contenido, from, to, 1);
            // Se asignan los adaptadores
            listViewNotas.setAdapter(adapterList);
            gridViewNotas.setAdapter(adapterGrid);
        }


    }

    // Se crea el menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflamos el option menu con nuestro layout
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        // Después de inflar, recogemos las referencias a los botones que nos interesan
        this.itemListView = menu.findItem(R.id.vistaLista);
        this.itemGridView = menu.findItem(R.id.vistaGrid);
        return true;
    }

    // Acciones que se realizaran al seleccionar un boton del la barra de menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Eventos para los clicks en los botones del options menu
        switch (item.getItemId()) {
            case R.id.vistaLista:
                opcion = this.SWITCH_TO_LIST_VIEW;
                this.switchListGridView(opcion);
                return true;
            case R.id.vistaGrid:
                opcion = this.SWITCH_TO_GRID_VIEW;
                this.switchListGridView(opcion);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    // Se cambia la vista entre un List y un Grid
    private void switchListGridView(int option) {
        // Método para cambiar entre Grid/List view
        if (option == SWITCH_TO_LIST_VIEW) {
            // Si queremos cambiar a list view, y el list view está en modo invisible...
            if (this.listViewNotas.getVisibility() == View.GONE) {
                // ... escondemos el grid view, y enseñamos su botón en el menú de opciones
                this.gridViewNotas.setVisibility(View.GONE);
                this.itemGridView.setVisible(true);
                // no olvidamos enseñar el list view, y esconder su botón en el menú de opciones
                if (contenido.getCount() != 0)
                    this.listViewNotas.setVisibility(View.VISIBLE);
                this.itemListView.setVisible(false);
            }
        } else if (option == SWITCH_TO_GRID_VIEW) {
            // Si queremos cambiar a grid view, y el grid view está en modo invisible...
            if (this.gridViewNotas.getVisibility() == View.GONE) {
                // ... escondemos el list view, y enseñamos su botón en el menú de opciones
                this.listViewNotas.setVisibility(View.GONE);
                this.itemListView.setVisible(true);
                // no olvidamos enseñar el grid view, y esconder su botón en el menú de opciones
                if (contenido.getCount() != 0)
                    this.gridViewNotas.setVisibility(View.VISIBLE);
                this.itemGridView.setVisible(false);
            }
        }
    }

    // Se crea el menuContext para borrar
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        // Inflamos el context menu con nuestro layout
        MenuInflater inflater = getMenuInflater();
        // Inflamos
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // Obtener info en el context menu del objeto que se pinche
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case R.id.borrarNota:
                baseDatos.borrarNota(info.id);
                establecerAdaptadores();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Como solo lanzamos una actividad, requestCode siempre será
        // SECONDARY_ACTIVITY_TAG. No hace falta que lo comprobemos.

        String mensajeResultado;

        if ((data == null)) {
            mensajeResultado = getResources().getString(R.string.cancelado);
        } else {
            // Tenemos una respuesta. La recuperamos del intent que nos
            // ha llegado desde la segunda actividad.

            int icono;
            String tipo = data.getStringExtra(NotasBDAdapter.CAMPO_CATEGORIA);

            switch (tipo) {
                case "REUNIÓN":
                    icono = R.mipmap.ic_reunion;
                    break;
                case "AVISO":
                    icono = R.mipmap.ic_aviso;
                    break;
                default:
                    icono = R.mipmap.ic_varios;
                    break;
            }

            if (esNuevaNota) {

                if (baseDatos.crearNota(data.getStringExtra(NotasBDAdapter.CAMPO_CATEGORIA),
                        data.getStringExtra(NotasBDAdapter.CAMPO_TITULO), data.getStringExtra(NotasBDAdapter.CAMPO_DESCRIPCION), icono) != -1) {
                    mensajeResultado = getResources().getString(R.string.altaCorrecto);
                } else {
                    mensajeResultado = getResources().getString(R.string.error);
                }
            } else {

                if (baseDatos.actualizarNota(data.getIntExtra(NotasBDAdapter.CAMPO_ID, -1), data.getStringExtra(NotasBDAdapter.CAMPO_CATEGORIA),
                        data.getStringExtra(NotasBDAdapter.CAMPO_TITULO), data.getStringExtra(NotasBDAdapter.CAMPO_DESCRIPCION), icono)) {
                    mensajeResultado = getResources().getString(R.string.modificacionCorrecto);
                } else {
                    mensajeResultado = getResources().getString(R.string.error);
                }

            }
            establecerAdaptadores();
            Toast.makeText(getApplicationContext(), mensajeResultado, Toast.LENGTH_LONG).show();
        }
    }


}
