package com.example.dany_.intentexplicito;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SegundaActividad extends AppCompatActivity {

    EditText _nombre;
    EditText _apellido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda_actividad);

        String nombre = getIntent().getStringExtra("nombre");
        String apellido = getIntent().getStringExtra("apellido");

        setResult(RESULT_CANCELED);

        if (nombre != null) {
            _nombre = findViewById(R.id.editTextNombre);
            _nombre.setText(nombre);

            _apellido = findViewById(R.id.editTextApellido);
            _apellido.setText(apellido);
        }

        Button _aceptar = findViewById(R.id.buttonAceptar);

        _aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResponder(v);
            }
        });


    }

    public void onResponder(View v) {

        // Sacamos del cuadro de texto la respuesta al saludo
        // escrita por el usuario.
        _nombre = findViewById(R.id.editTextNombre);

        _apellido = findViewById(R.id.editTextApellido);

        String nombre = _nombre.getText().toString();
        String apellido = _apellido.getText().toString();

        // Creamos un nuevo intent donde enviaremos de vuelta
        // los datos de nuestra ejecución.
        Intent datos = new Intent();
        // Metemos como datos extra del intent la respuesta del
        // usuario.
        datos.putExtra("nombre", nombre);
        datos.putExtra("apellido", apellido);
        // Le decimos a Android que estamos preparados para acabar
        // con éxito...
        setResult(RESULT_OK, datos);

        // ... y le pedimos que nos cierre.
        finish();

    } // onResponder
}
