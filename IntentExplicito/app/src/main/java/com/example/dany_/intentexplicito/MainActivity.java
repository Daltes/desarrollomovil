package com.example.dany_.intentexplicito;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button _Atla;
    Intent i;
    Button _Modificar;
    TextView _nombre;
    TextView _apellido;
    boolean seHaDadoAlta = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _nombre = findViewById(R.id.textViewMensajeNombre);
        _apellido = findViewById(R.id.textViewMensajeApellidos);
        _Atla = findViewById(R.id.buttonAlta);

        _Atla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(MainActivity.this, SegundaActividad.class);

                startActivityForResult(i, SECONDARY_ACTIVITY_TAG);

                _Modificar.setEnabled(true);
                _Atla.setEnabled(false);

            }
        });

        _Modificar = findViewById(R.id.buttonModificar);

        _Modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = new Intent(MainActivity.this, SegundaActividad.class);

                String nombre = _nombre.getText().toString().split(":")[_nombre.getText().toString().split(":").length - 1];
                String apellido = _apellido.getText().toString().split(":")[_apellido.getText().toString().split(":").length - 1];

                i.putExtra("nombre", nombre);
                i.putExtra("apellido", apellido);

                startActivityForResult(i, SECONDARY_ACTIVITY_TAG);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        String respuesta;
        _nombre = findViewById(R.id.textViewMensajeNombre);
        _apellido = findViewById(R.id.textViewMensajeApellidos);


        // Como solo lanzamos una actividad, requestCode siempre será
        // SECONDARY_ACTIVITY_TAG. No hace falta que lo comprobemos.

        if ((data == null)) {
                respuesta = getResources().getString(R.string.error);
                if(!seHaDadoAlta){
                    _Modificar.setEnabled(false);
                    _Atla.setEnabled(true);
                }
        } else {
            // Tenemos una respuesta. La recuperamos del intent que nos
            // ha llegado desde la segunda actividad.
            respuesta = getResources().getString(R.string.correcto);

            if(!(data.getStringExtra("nombre").trim().equals("") || data.getStringExtra("apellido").trim().equals(""))){
                String nombre = getResources().getString(R.string.nombre) + getResources().getString(R.string.dos_puntos) + data.getStringExtra("nombre");
                String apellido = getResources().getString(R.string.apellidos) + getResources().getString(R.string.dos_puntos) + data.getStringExtra("apellido");

                seHaDadoAlta = true;

                _nombre.setText(nombre);
                _apellido.setText(apellido);
            }else{
                respuesta = getResources().getString(R.string.error_Vacio);
                if(!seHaDadoAlta){
                    _Modificar.setEnabled(false);
                    _Atla.setEnabled(true);
                }
            }

        }
        // Mostramos un toast con la respuesta o con la queja.
        Context contexto = getApplicationContext();
        Toast.makeText(contexto, respuesta, Toast.LENGTH_LONG).show();
    }

    /**
     * "Etiqueta" que usamos para llamar a la segunda actividad, y que
     * esperamos recibir como primer parámetro de vuelta en el
     * callback onActivityResult().
     */
    private static final int SECONDARY_ACTIVITY_TAG = 1;
}
