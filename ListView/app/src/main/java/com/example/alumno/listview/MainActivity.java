package com.example.alumno.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Opcion> datos = new ArrayList<Opcion>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Asignamos el objeto ListView con el id de la etiqueta ListView del
        //layout principal
        ListView listaOpciones = (ListView) findViewById(R.id.ListaOpciones);

        // Creamos los objetos y los guardamos en un array
        datos.add(new Opcion("Atletismo", R.drawable.atletismo, false, true));
        datos.add(new Opcion("Baloncesto", R.drawable.baloncesto, false, true));
        datos.add(new Opcion("Fútbol", R.drawable.futbol, false, true));
        datos.add(new Opcion("Golf", R.drawable.golf, false, true));
        datos.add(new Opcion("Motociclismo", R.drawable.motociclismo, false, true));
        datos.add(new Opcion("Natación", R.drawable.natacion, false, false));
        datos.add(new Opcion("pingpong", R.drawable.pingpong, false, true));

        // Usamos un adaptador para dibujar las opciones de la lista
        final AdaptadorOpciones adaptador = new AdaptadorOpciones(MainActivity.this, datos);

        // Establecemos el adaptador del Listview
        listaOpciones.setAdapter(adaptador);

        // Definimos el evento setOnItemClick
        listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            // Si se hace clic sobre una opción mostramos un mensaje
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                datos.get(position).setCheckBox(!datos.get(position).isCheckBox());
                adaptador.notifyDataSetChanged();
            }
        });

        Button _boton = findViewById(R.id.boton);

        _boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String resultado = getResources().getString(R.string.resultado);
                String opcionesSeleccionandas = "";
                for (int i = 0; i < datos.size(); i++) {
                    if (datos.get(i).isCheckBox()) {
                        if (datos.get(i).isMasculino()) {
                            opcionesSeleccionandas += getResources().getString(R.string.articulo_masculino) + getResources().getString(R.string.espacio) + datos.get(i).getNombre();
                        } else {
                            opcionesSeleccionandas += getResources().getString(R.string.articulo_femenino) + getResources().getString(R.string.espacio) + datos.get(i).getNombre();
                        }
                        opcionesSeleccionandas += getResources().getString(R.string.coma) + getResources().getString(R.string.espacio);
                    }
                }
                opcionesSeleccionandas = aniadirConjuncionCopulativa(opcionesSeleccionandas);
                resultado = String.format(resultado, opcionesSeleccionandas);
                Toast.makeText(MainActivity.this, resultado, Toast.LENGTH_LONG).show();
            }

            private String aniadirConjuncionCopulativa(String opcionesSeleccionandas) {
                String separacion[] = opcionesSeleccionandas.split(getResources().getString(R.string.coma) + getResources().getString(R.string.espacio));
                String resultado = "";
                boolean esPrimeraVez = true;
                for (int i = 0; i < separacion.length; i++) {
                    if (esPrimeraVez) {
                        resultado += separacion[i];
                        esPrimeraVez = false;
                    } else {
                        if (i == separacion.length - 1) {
                            resultado += getResources().getString(R.string.espacio) +
                                    getResources().getString((R.string.conjuncion_copulativa)) +
                                    getResources().getString(R.string.espacio) + separacion[i];
                        } else {
                            resultado += getResources().getString(R.string.coma) +
                                    getResources().getString(R.string.espacio) + separacion[i];
                        }
                    }
                }
                return resultado;
            }
        });


    }

}
