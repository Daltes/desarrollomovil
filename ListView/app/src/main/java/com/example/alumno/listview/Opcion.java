package com.example.alumno.listview;

public class Opcion {
    //Atributos
    private String nombre;
    private int icono;
    private boolean checkBox;
    private boolean masculino;
    //Fin de atributos


    public Opcion(String nombre, int icono, boolean checkBox, boolean masculino) {
        this.nombre = nombre;
        this.icono = icono;
        this.checkBox = checkBox;
        this.masculino = masculino;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIcono() {
        return icono;
    }

    public void setIcono(int icono) {
        this.icono = icono;
    }

    public boolean isCheckBox() {
        return checkBox;
    }

    public void setCheckBox(boolean checkBox) {
        this.checkBox = checkBox;
    }

    public boolean isMasculino() {
        return masculino;
    }

    public void setMasculino(boolean masculino) {
        this.masculino = masculino;
    }
}
