package com.example.alumno.listview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptadorOpciones extends ArrayAdapter<Opcion> {

    Activity contexto;

    // Contructor del adaptador usando el contexto de la aplicacion actual
    AdaptadorOpciones(Activity contexto, ArrayList<Opcion> datos) {
        super(contexto, R.layout.listitem_opcion, datos);
        this.contexto = contexto;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        View item = convertView;

        ViewHolder holder;

        if (item == null) {
            LayoutInflater inflater = contexto.getLayoutInflater();
            item = inflater.inflate(R.layout.listitem_opcion, null);

            holder = new ViewHolder();
            holder.caja = item.findViewById(R.id.checkBoxCaja);
            holder.imagenDeporte = item.findViewById(R.id.imageViewImagen);
            holder.texto = item.findViewById(R.id.textViewEtiqueta);
            item.setTag(holder);
        } else {
            holder = (ViewHolder) item.getTag();
        }

        //Mediante getItem cargamos cada uno de los objetos del array
        Opcion mielemento = getItem(position);

        // Asignamos los valores
        holder.texto.setText(mielemento.getNombre());
        holder.imagenDeporte.setImageResource(mielemento.getIcono());
        holder.caja.setChecked(mielemento.isCheckBox());

        return item;
    }

    class ViewHolder {
        CheckBox caja;
        ImageView imagenDeporte;
        TextView texto;
    }

}
