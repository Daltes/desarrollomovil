package com.example.dany_.menus;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptadorOpciones extends ArrayAdapter<Opcion> {

    Activity contexto;

    // Contructor del adaptador usando el contexto de la aplicacion actual
    AdaptadorOpciones(Activity contexto, ArrayList<Opcion> datos) {
        super(contexto, R.layout.listacompra, datos);
        this.contexto = contexto;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        View item = convertView;

        ViewHolder holder;

        if (item == null) {
            LayoutInflater inflater = contexto.getLayoutInflater();
            item = inflater.inflate(R.layout.listacompra, null);

            holder = new ViewHolder();
            holder.texto = item.findViewById(R.id.textViewObjeto);

            item.setTag(holder);
        } else {
            holder = (ViewHolder) item.getTag();
        }




        //Mediante getItem cargamos cada uno de los objetos del array
        Opcion mielemento = getItem(position);

        // Asignamos los valores
        holder.texto.setText(mielemento.getNombre());

        if (mielemento.isComprado()) {
            holder.texto.setPaintFlags(holder.texto.getPaintFlags() |
                    Paint.STRIKE_THRU_TEXT_FLAG);
            holder.texto.setTextColor(Color.parseColor("#00FF00"));
        }else{
            holder.texto.setPaintFlags(holder.texto.getPaintFlags()
                    &~Paint.STRIKE_THRU_TEXT_FLAG);
            holder.texto.setTextColor(Color.parseColor("#FF0000"));
        }

        return item;
    }

    class ViewHolder {
        TextView texto;
    }
}
