package com.example.dany_.menus;

import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements EditDFragment.EditNameDialogListener {
    private ArrayList<Opcion> datos = new ArrayList<Opcion>();
    private ListView listadoPrincipal;
    private ArrayAdapter<String> adaptador;
    FragmentManager fm = getSupportFragmentManager();

    // Barra de menu
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    // Menu contextual
    @Override
    // Método donde definimos el menú contextual cuando se despliega
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        //Inflador del menú contextual
        MenuInflater inflater = getMenuInflater();

        // Si el componente que vamos a dibujar es el ListView usamos
        // el fichero XML correspondiente
        if (v.getId() == R.id.listaCompra) {
            AdapterView.AdapterContextMenuInfo info =
                    (AdapterView.AdapterContextMenuInfo) menuInfo;
            // Definimos la cabecera del menú contextual
            Opcion titulo = (Opcion) listadoPrincipal.getAdapter().getItem(info.position);

            menu.setHeaderTitle(getResources().getString(R.string.TituloMenuContexOpciones) + getResources().getString(R.string.espacio) + titulo.getNombre());
            // Inflamos el menú contextual
            inflater.inflate(R.menu.menu_contextual_lista, menu);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ListView listaOpciones = (ListView) findViewById(R.id.listaCompra);

        datos.add(new Opcion("leche", true));
        datos.add(new Opcion("galletas", true));
        datos.add(new Opcion("patatas", false));

        adaptador = (ArrayAdapter) new AdaptadorOpciones(MainActivity.this, datos);

        listaOpciones.setAdapter(adaptador);

        listadoPrincipal = (ListView) findViewById(R.id.listaCompra);
        listadoPrincipal.setAdapter(adaptador);

        registerForContextMenu(listadoPrincipal);

        listaOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            // Si se hace clic sobre una opción mostramos un mensaje
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                TextView texto = (TextView) view.findViewById(R.id.textViewObjeto);
                datos.get(position).setComprado(!datos.get(position).isComprado());
                if (datos.get(position).isComprado()) {
                    texto.setPaintFlags(texto.getPaintFlags() |
                            Paint.STRIKE_THRU_TEXT_FLAG);
                    texto.setTextColor(Color.parseColor("#00FF00"));
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.comprar)
                            + getResources().getString(R.string.espacio) + datos.get(position).getNombre(), Toast.LENGTH_SHORT).show();
                } else {
                    texto.setPaintFlags(texto.getPaintFlags()
                            & ~Paint.STRIKE_THRU_TEXT_FLAG);
                    texto.setTextColor(Color.parseColor("#FF0000"));
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.no_comprar)
                            + getResources().getString(R.string.espacio) + datos.get(position).getNombre(), Toast.LENGTH_SHORT).show();
                }
                adaptador.notifyDataSetChanged();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        EditDFragment editdFragment = new EditDFragment();
        // Show Alert DialogFragment
        editdFragment.show(fm, "Edit Dialog Fragment");
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String name, boolean esNuevo, int posicion) {

        name = name.trim();

        if (name.equals("")) {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.articulo_vacio), Toast.LENGTH_SHORT).show();
            return;
        }

        if (esNuevo) {
            datos.add(new Opcion(name, false));

        } else {
            datos.get(posicion).setNombre(name);
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }

    @Override
    // Si el usuario selecciona una opción del menú contextual mostramos
    // la opción seleccionada en la etiqueta
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            // Se selecciona la opción "Editar texto" de menú contextual de la etiqueta
            case R.id.menuItemEditar:
                Bundle bundle = new Bundle();
                bundle.putString("titulo", getResources().getString(R.string.edit_fragment_modificar_titulo));
                bundle.putString("texto", getResources().getString(R.string.edit_fragment_modificar_texto));
                bundle.putInt("posicion", info.position);
                bundle.putString("articulo", datos.get(info.position).getNombre());
                EditDFragment editdFragment = new EditDFragment();
                // Show Alert DialogFragment
                editdFragment.setArguments(bundle);
                editdFragment.show(fm, "Edit Dialog Fragment");
                return true;
            // Se selecciona la opción "Borrar" de menú contextual de la etiqueta
            case R.id.menuItemBorrar:
                datos.remove(info.position);
                adaptador.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
