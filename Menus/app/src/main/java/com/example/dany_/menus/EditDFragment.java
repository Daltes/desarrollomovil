package com.example.dany_.menus;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EditDFragment extends DialogFragment {
    public interface EditNameDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, String name,boolean esNuevo,int posicion);

        public void onDialogNegativeClick(DialogFragment dialog);
    }

    private AlertDialog.Builder ventana;

    public EditDFragment() {
        // Empty constructor required for DialogFragment
    }


    EditNameDialogListener mListener;

    //private Handler mResponseHandler;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (EditNameDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ShareDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View v = inflater.inflate(R.layout.edit_fragment_nuevo_articulo, null);
        final EditText nombreEdit = (EditText) v.findViewById(R.id.editTextArticulo);
        final TextView tvTexto = (TextView) v.findViewById(R.id.textViewTexto);

        String titulo = getResources().getString(R.string.Titulo_EditDFragment);
        String texto = getResources().getString(R.string.texto_editDFragment);
        boolean esNuevo = true;
        int posicion = 0;
        final Bundle bundle = getArguments();
        if (bundle != null) {
            texto = bundle.getString("texto");
            titulo = bundle.getString("titulo");
            esNuevo = false;
            nombreEdit.setText(bundle.getString("articulo"));
            posicion = bundle.getInt("posicion");
        }
        tvTexto.setText(texto);


        final boolean finalEsNuevo = esNuevo;
        final int finalPosicion = posicion;
        return new AlertDialog.Builder(getActivity())

                // Set Dialog Title
                .setTitle(titulo)
                //Set XML file
                .setView(v)

                // Positive button
                .setPositiveButton(getResources().getString(R.string.aceptar_EditDFragment), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String name = nombreEdit.getText().toString();
                        mListener.onDialogPositiveClick(EditDFragment.this, name, finalEsNuevo,finalPosicion);

                    }
                })

                // Negative Button
                .setNegativeButton(getResources().getString(R.string.cancelar_EditDFragment), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.cancelar), Toast.LENGTH_SHORT).show();
                    }
                }).create();
    }
}
