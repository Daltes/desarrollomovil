package com.example.alumno.enviarperfil;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private final int SELECT_IMAGE = 100;
    boolean imagenCambiada = false;
    private int miProgreso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button _botonSeleccionar = findViewById(R.id.buttonSeleccionarImagen);


        _botonSeleccionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // comprobar version actual de android que estamos corriendo
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    // Comprobar si ha aceptado, no ha aceptado, o nunca se le ha preguntado
                    if (CheckPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Ha aceptado
                        Intent i = new Intent();
                        i.setType("image/*");
                        i.setAction(Intent.ACTION_PICK);
                        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                            return;
                        // startActivity(i);
                        startActivityForResult(Intent.createChooser(i, getResources().getString(R.string.seleccionar_imagen)), SELECT_IMAGE);
                    } else {
                        // Ha denegado o es la primera vez que se le pregunta
                        if (!shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            // No se le ha preguntado aún
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, SELECT_IMAGE);
                        } else {
                            // Ha denegado
                            Toast.makeText(MainActivity.this, getResources().getString(R.string.permisos), Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            i.addCategory(Intent.CATEGORY_DEFAULT);
                            i.setData(Uri.parse("package:" + getPackageName()));
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            startActivity(i);
                        }
                    }
                } else {
                    OlderVersions();
                }
            }

            private void OlderVersions() {
                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(i, getResources().getString(R.string.seleccionar_imagen)), SELECT_IMAGE);
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(i);
                } else {
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.permisoDenegado), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button _botonEnviar = findViewById(R.id.buttonEnviar);
        final EditText _descripcion = findViewById(R.id.editTextDescripcion);

        _botonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagenCambiada && _descripcion.getText().length() != 0) {
                    DialogoProgreso enviado = new DialogoProgreso();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    enviado.show(fragmentManager, "tagAlerta");

                }else{
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean CheckPermission(String permission) {
        int result = this.checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == MainActivity.RESULT_OK) {
                if (data != null) {
                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(MainActivity.this.getContentResolver(), data.getData());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ImageView _Imagen = findViewById(R.id.imageViewPerfil);
                    _Imagen.setImageBitmap(bitmap);
                    imagenCambiada = true;
                }
            } else if (resultCode == MainActivity.RESULT_CANCELED) {
                Toast.makeText(MainActivity.this, getResources().getString(R.string.cancelar), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
