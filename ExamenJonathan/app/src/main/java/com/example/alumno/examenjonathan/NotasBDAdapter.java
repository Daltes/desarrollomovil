package com.example.alumno.examenjonathan;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class NotasBDAdapter {

    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_TITULO = "TITULO";
    public static final String CAMPO_AUTOR = "AUTOR";
    public static final String CAMPO_PORTADA = "PORTADA";
    public static final String CAMPO_FAVORITO = "FAVORITO";
    public static final String TABLA_BD = "LIBROS";
    private Context contexto;
    private SQLiteDatabase baseDatos;
    private NotasBDHelper bdHelper;

    public NotasBDAdapter(Context context) {
        this.contexto = context;
    }

    public NotasBDAdapter abrir() throws SQLException {
        NotasBDHelper usdbh =
                new NotasBDHelper(contexto);

        this.baseDatos = usdbh.getWritableDatabase();
        return this;
    }

    public void cerrar() {
        bdHelper.close();
    }

    public long crearNota(String titulo, String autor, String portada, int favorito) {
        ContentValues nuevaNota = new ContentValues();
        nuevaNota.put(CAMPO_TITULO, titulo);
        nuevaNota.put(CAMPO_AUTOR, autor);
        nuevaNota.put(CAMPO_PORTADA, portada);
        nuevaNota.put(CAMPO_FAVORITO, favorito);
        return baseDatos.insert(TABLA_BD, null, nuevaNota);
    }

    public boolean actualizarLibro(long id, String titulo, String autor, String portada, int favorito) {
        ContentValues nuevaNota = new ContentValues();
        nuevaNota.put(CAMPO_TITULO, titulo);
        nuevaNota.put(CAMPO_AUTOR, autor);
        nuevaNota.put(CAMPO_PORTADA, portada);
        nuevaNota.put(CAMPO_FAVORITO, favorito);
        return baseDatos.update(TABLA_BD, nuevaNota, CAMPO_ID + "=" + id, null) != 0;
    }

    public boolean borrarLibro(long id) {
        return baseDatos.delete(TABLA_BD, CAMPO_ID + "=" + id, null) != 0;
    }

    public Cursor obtenerLibros() {
        return baseDatos.rawQuery("SELECT * FROM " + TABLA_BD, null);
    }

    public Cursor getLibro(long id) throws SQLException {
        return baseDatos.rawQuery("SELECT * FROM " + TABLA_BD + " WHERE " + CAMPO_ID + "=" + id, null);
    }

    public Cursor obtenerFavoritos() {
    return  baseDatos.rawQuery("SELECT * FROM " + TABLA_BD + " WHERE " + CAMPO_FAVORITO + "=" + 0,null);
    }

    public Cursor obtenerAutor() {
        return  baseDatos.rawQuery("SELECT "+CAMPO_AUTOR+" FROM " + TABLA_BD ,null);
    }

    public Cursor obtenerAutor(String nombreAutor) {
        return  baseDatos.rawQuery("SELECT * FROM " + TABLA_BD + " WHERE " + CAMPO_AUTOR + "=" + "'"+nombreAutor+"'",null);
    }

    public boolean cambiarFavorito(long id, int favorito){
        ContentValues nuevaNota = new ContentValues();
        nuevaNota.put(CAMPO_FAVORITO, favorito);
        return baseDatos.update(TABLA_BD, nuevaNota, CAMPO_ID + "=" + id, null) != 0;
    }

}
