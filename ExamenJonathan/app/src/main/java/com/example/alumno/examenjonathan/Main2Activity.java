package com.example.alumno.examenjonathan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        int titulo = getIntent().getIntExtra("imagen",0);

        if(titulo != 0){
            ImageView v = (ImageView)findViewById(R.id.imagenPortada);
            v.setImageResource(titulo);
        }

    }
}
