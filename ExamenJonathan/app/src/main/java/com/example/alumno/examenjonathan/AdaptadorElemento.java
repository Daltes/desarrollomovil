package com.example.alumno.examenjonathan;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptadorElemento extends ArrayAdapter<Elemento> {
    Activity contexto;

    // Contructor del adaptador usando el contexto de la aplicacion actual

    AdaptadorElemento(Activity contexto, ArrayList<Elemento> datos) {
        super(contexto, R.layout.list_item, datos);
        this.contexto = contexto;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View item = convertView;

        ViewHolder holder;

        if (item == null) {
            LayoutInflater inflater = contexto.getLayoutInflater();
            item = inflater.inflate(R.layout.list_item, null);

            holder = new ViewHolder();
            holder.titulo = item.findViewById(R.id.LblTitulo);
            holder.autor = item.findViewById(R.id.LblAutor);
            holder.icono = item.findViewById(R.id.icono);

            item.setTag(holder);
        } else {
            holder = (ViewHolder) item.getTag();
        }

        //Mediante getItem cargamos cada uno de los objetos del array
        Elemento mielemento = getItem(position);

        // Asignamos los valores
        holder.titulo.setText(mielemento.getTitulo());
        holder.autor.setText(mielemento.getAutor());

        if (mielemento.getFavorito() == 0) {
            holder.titulo.setPaintFlags(holder.titulo.getPaintFlags());
            holder.titulo.setTextColor(Color.parseColor("#FF0000"));
        }else{
            holder.titulo.setPaintFlags(holder.titulo.getPaintFlags()
                    &~Paint.STRIKE_THRU_TEXT_FLAG);
            holder.titulo.setTextColor(Color.parseColor("#00FF00"));
        }


        if(mielemento.getPortada() == 0){
            holder.icono.setImageResource(R.mipmap.ic_imagen_no);
        }else{
            holder.icono.setImageResource(R.mipmap.ic_imagen_si);
        }

        return item;
    }
    class ViewHolder {
        TextView titulo;
        TextView autor;
        ImageView icono;
    }

}

