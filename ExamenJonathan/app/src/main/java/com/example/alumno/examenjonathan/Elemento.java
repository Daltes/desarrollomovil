package com.example.alumno.examenjonathan;

public class Elemento {

    int _id;

    String titulo;
    String autor;
    int portada;
    int favorito;
    int imagen;

    public Elemento(int _id, String titulo, String autor, int portada, int favorito, int imagen) {
        this._id = _id;
        this.titulo = titulo;
        this.autor = autor;
        this.portada = portada;
        this.favorito = favorito;
        this.imagen = imagen;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getPortada() {
        return portada;
    }

    public void setPortada(int portada) {
        this.portada = portada;
    }

    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }
}
