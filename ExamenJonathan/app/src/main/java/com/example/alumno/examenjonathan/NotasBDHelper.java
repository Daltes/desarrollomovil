package com.example.alumno.examenjonathan;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class NotasBDHelper extends SQLiteOpenHelper {

    private static final String BD_NOMBRE = "bdnotas.bd";
    private static final int BD_VERSION = 1;
    Context contexto;

    public NotasBDHelper(Context context) {
        super(context, BD_NOMBRE, null, BD_VERSION);
        this.contexto = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        crearTabla(db);
    }

    private void crearTabla(SQLiteDatabase db) {
        InputStream frawTituloAutor = contexto.getResources().openRawResource(R.raw.tituloautor);
        // Lectura del fichero
        try {
            BufferedReader brTituloAutor = new BufferedReader(new InputStreamReader(frawTituloAutor));

            String linea;
            while ((linea = brTituloAutor.readLine()) != null) {
                db.execSQL(linea);
            }

            frawTituloAutor.close();
            brTituloAutor.close();

        } catch (Exception e) {
            Log.e("Ficheros", "Error al leer fichero desde recurso raw");
        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + NotasBDAdapter.TABLA_BD);
        crearTabla(db);
    }
}
