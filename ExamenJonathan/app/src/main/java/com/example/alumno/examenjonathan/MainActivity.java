package com.example.alumno.examenjonathan;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView _listViewlistalibros;
    private NotasBDAdapter baseDatos;
    private Cursor contenido;
    private ArrayAdapter<String> adaptador;
    private Spinner _spinner;
    ArrayList<Elemento> datos;
    private static final int SECONDARY_ACTIVITY_TAG = 1;


    // Menu contextual
    @Override
    // Método donde definimos el menú contextual cuando se despliega
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        //Inflador del menú contextual
        MenuInflater inflater = getMenuInflater();

        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) menuInfo;

        menu.setHeaderTitle(getResources().getString(R.string.titulo_menu_contextual) +
                " " + _listViewlistalibros.getAdapter().getItem((int) info.id));



        int tienePortada = datos.get((int) info.id).getPortada();

        inflater.inflate(R.menu.menu_contextual_lista, menu);

        MenuItem mi = menu.findItem(R.id.menuItemMostrar);
        if (tienePortada == 0) {

            mi.setTitle(getResources().getString(R.string.no_portada));

        } else {
            mi.setTitle(getResources().getString(R.string.ver_portada));
        }

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _listViewlistalibros = findViewById(R.id.listalibros);

        registerForContextMenu(_listViewlistalibros);

        // Se inicia la base de datos
        baseDatos = new NotasBDAdapter(getApplicationContext());
        baseDatos = baseDatos.abrir();

        establecerAdaptadores(null);

        _spinner = findViewById(R.id.spinner_autor);
        ArrayList<String> autor = new ArrayList<>();

        autor.add(getResources().getString(R.string.spinner_header));

        Cursor autores = baseDatos.obtenerFavoritos();
        if (autores.moveToFirst()) {
            do {

                autor.add(autores.getString(autores.getColumnIndex(NotasBDAdapter.CAMPO_AUTOR)));

            }
            while (autores.moveToNext());
        }
        String[] arrayautor = autor.toArray(new String[0]);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayautor);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        _spinner.setAdapter(spinnerArrayAdapter);

        _spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent,
                                               android.view.View v, int position, long id) {


                        if (position == 0) {
                            establecerAdaptadores(null);
                        } else {

                            Cursor c = baseDatos.obtenerAutor((String) _spinner.getItemAtPosition(position));
                            establecerAdaptadores(c);
                        }

                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });



        _listViewlistalibros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            // Si se hace clic sobre una opción mostramos un mensaje
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                TextView texto = view.findViewById(R.id.LblTitulo);
                int favorito = datos.get(position).getFavorito();
                if (favorito == 0) {
                    texto.setPaintFlags(texto.getPaintFlags()
                            & ~Paint.STRIKE_THRU_TEXT_FLAG);
                    texto.setTextColor(Color.parseColor("#FF0000"));
                    baseDatos.cambiarFavorito(datos.get(position).get_id(),1);
                } else {
                    texto.setPaintFlags(texto.getPaintFlags() |
                            Paint.STRIKE_THRU_TEXT_FLAG);
                    texto.setTextColor(Color.parseColor("#00FF00"));
                    baseDatos.cambiarFavorito(datos.get(position).get_id(),0);
                }
                contenido.requery();
                establecerAdaptadores(contenido);
                adaptador.notifyDataSetChanged();
            }
        });


    }


    private void establecerAdaptadores(Cursor c) {

        if (c == null) {
            contenido = baseDatos.obtenerLibros();
        } else {
            contenido = c;
        }

       datos = new ArrayList<>();


        contenido.moveToFirst();
        do {
            datos.add(new Elemento(contenido.getInt(0),
                    contenido.getString(1),
                    contenido.getString(2),
                    contenido.getInt(3),
                    contenido.getInt(4),
                    0));
        }while (contenido.moveToNext());

        adaptador = (ArrayAdapter) new AdaptadorElemento(MainActivity.this, datos);
        _listViewlistalibros.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // Obtener info en el context menu del objeto que se pinche
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case R.id.menuItemMostrar:

                int imagen = getApplicationContext().getResources().getIdentifier(
                        "p" + String.valueOf(datos.get((int) info.id).get_id()),
                        "drawable", getApplicationContext().getPackageName());


                if (imagen != 0) {
                    Intent i = new Intent(MainActivity.this, Main2Activity.class);

                    i.putExtra("imagen", imagen);
                    startActivityForResult(i, SECONDARY_ACTIVITY_TAG);
                }


                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

}
